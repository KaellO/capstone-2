using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BurnThorns : MonoBehaviour
{
    float BurnSpeed = 0.01f;
    float BurnRate = 0.3f;
    [SerializeField] float BottomCutOff;
    [SerializeField] float TopCutOff;
    MaterialPropertyBlock propBlock;
    SpriteRenderer spriteRend;

    float currentCut;
    float cutoffRef;

    bool burning = false;
    private void Awake()
    {
        spriteRend = GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        propBlock = new MaterialPropertyBlock();

        spriteRend.GetPropertyBlock(propBlock);
        //The "-" is the minimum range, the multiply is the max range.
        propBlock.SetFloat("_Cutoff_Height", TopCutOff);
        currentCut = propBlock.GetFloat("_Cutoff_Height");
        cutoffRef = propBlock.GetFloat("_Cutoff_Height");
    }

    public void Burn()
    {
        if (burning)
            return;

        burning = true;
        StartCoroutine(StartBurning());
    }
    IEnumerator StartBurning()
    {
        while (cutoffRef > BottomCutOff)
        {
            spriteRend.GetPropertyBlock(propBlock);
            //The "-" is the minimum range, the multiply is the max range.
            //currentCut -= BurnRate;
            currentCut = Mathf.Lerp(currentCut, -10f, Time.deltaTime * BurnRate); 
            propBlock.SetFloat("_Cutoff_Height", currentCut);

            cutoffRef = propBlock.GetFloat("_Cutoff_Height");
            spriteRend.SetPropertyBlock(propBlock);
            yield return new WaitForSeconds(BurnSpeed);
        }

        transform.parent.gameObject.SetActive(false);
    }
}
