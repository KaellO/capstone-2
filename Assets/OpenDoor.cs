using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public Vector3 targetAngle = new Vector3(0f, -120f, 0f);

    private Vector3 currentAngle;

    bool opening = false;

    public void Start()
    {
        currentAngle = transform.eulerAngles;
    }

    public void Open()
    {
        if (opening)
            return;

        opening = true;
        StartCoroutine(StartOpening());
    }

    IEnumerator StartOpening()
    {
        while (currentAngle != targetAngle)
        {
            currentAngle = new Vector3(
                Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime),
                Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime),
                Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime));

            transform.eulerAngles = currentAngle;
            yield return null;
        }

    }
}
